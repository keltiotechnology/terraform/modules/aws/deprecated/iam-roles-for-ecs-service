variable "iam_role_name" {
  type    = string
  default = "ecsInstanceRole"
}

variable "iam_task_execution_role_name" {
  type    = string
  default = "ecsTaskExecutionRole"
}

variable "allowed_secrets_arn" {
  type        = list(string)
  description = "List of secrets arn that iam task execution role will have access"
  default     = []
}
