/*
 * Docs: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/instance_IAM_role.html
*/

/*
 * IAM role and policies
*/

resource "aws_iam_role" "ecs_service_role" {
  name = var.iam_role_name
  assume_role_policy = jsonencode({
    "Version" : "2008-10-17",
    "Statement" : [
      {
        "Action" : "sts:AssumeRole",
        "Principal" : {
          "Service" : [
            "ecs.amazonaws.com",
            "ec2.amazonaws.com"
          ]
        },
        "Effect" : "Allow"
      }
    ]
  })

  provisioner "local-exec" {
    when    = create
    working_dir = path.module
    command = <<-EOT
    echo Starting creating empty cluster for service role
    python script/create_AWSServiceRoleForECS.py
    echo Complete creating empty cluster for service role
    EOT
  }    
}

resource "aws_iam_instance_profile" "default" {
  name = "${var.iam_role_name}_profile"
  path = "/"
  role = aws_iam_role.ecs_service_role.name
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role" {
  role       = aws_iam_role.ecs_service_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}


resource "aws_iam_role_policy" "ecs_service_role-policy" {
  name = "${var.iam_role_name}_ecs_service_role_policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "elasticloadbalancing:Describe*",
          "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
          "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
          "ec2:Describe*",
          "ec2:AuthorizeSecurityGroupIngress",
          "elasticloadbalancing:RegisterTargets",
          "elasticloadbalancing:DeregisterTargets",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        "Resource" : [
          "*"
        ]
      }
    ]
    }
  )
  role = aws_iam_role.ecs_service_role.id
}


#* https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_execution_IAM_role.html
resource "aws_iam_role" "ecsTaskExecutionRole" {
  name               = var.iam_task_execution_role_name
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy" "ecs_task_execution_role_policy" {
  name = "${var.iam_task_execution_role_name}_ecs_service_role_policy"
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "secretsmanager:GetSecretValue"
        ],
        "Resource" : var.allowed_secrets_arn
      }
    ]
    }
  )
  role = aws_iam_role.ecsTaskExecutionRole.id
}

#* This AWS managed policy allows pulling private repo from ECR
resource "aws_iam_role_policy_attachment" "ecsTaskExecutionRole_policy" {
  role       = aws_iam_role.ecsTaskExecutionRole.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
