output "aws_iam_role_arn" {
  value = aws_iam_role.ecs_service_role.arn
}
output "aws_iam_role_name" {
  value = aws_iam_role.ecs_service_role.name
}
output "aws_iam_instance_profile_name" {
  value = aws_iam_instance_profile.default.name
}
